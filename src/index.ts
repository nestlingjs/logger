import * as Logger from 'bunyan'
import {
  Injectable,
  DynamicModule,
  Global,
  Module
} from '@nestjs/common'

@Injectable()
export class LogService extends Logger {}

export function createLoggerProvider (options) {
  return {
    provide: LogService,
    useFactory: () => new LogService(options)
  }
}
@Global()
@Module({})
export class LoggerModule {
  static forRoot (options): DynamicModule {
    const provider = createLoggerProvider(options)
    return {
      module: LoggerModule,
      providers: [provider],
      exports: [provider]
    }
  }
}
