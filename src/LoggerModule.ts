import { DynamicModule, Global, Module } from '@nestjs/common'
import { createLoggerProvider } from './createLoggerProvider'

@Global()
@Module({})
export class LoggerModule {
  static forRoot (options): DynamicModule {
    const provider = createLoggerProvider(options)
    return {
      module: LoggerModule,
      providers: [provider],
      exports: [provider]
    }
  }
}
