import * as Logger from 'bunyan'
import { Injectable } from '@nestjs/common'

@Injectable()
export class LogService extends Logger {}
