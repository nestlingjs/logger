import { LogService } from './LogService'

export function createLoggerProvider (options) {
  return {
    provide: LogService,
    useFactory: () => new LogService(options)
  }
}
