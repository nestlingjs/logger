# Nestling logger module

Simple logger wrapper

Usage:

```typescript
import { LoggerModule } from '@nestling/logger'

@Module({
  imports: [
    LoggerModule.forRoot({
      name: environment.client.id
    }),
    ...
  ]
})
export class ApplicationModule {}
```
